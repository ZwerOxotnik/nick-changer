# Nickname changer

Read this in another language | [English](/README.md) | [Русский](/docs/ru/README.md)
|---|---|---|

## Quick Links

[Changelog](/changelog.txt) | [Contributing](CONTRIBUTING.md)
| --- | --- |

## Contents

* [Overview](#overview)
* [Issues](#issue)
* [Features](#feature)
* [Installing](#installing)
* [License](#license)

## Overview

Adds mod settings, "/nick" command, mod interface for changing your nickname.

## <a name="issue"></a> Found an Issue?

Please report any issues or a mistake in the documentation, you can help us by
[submitting an issue](https://gitlab.com/ZwerOxotnik/nick-changer/issues) to our GitLab Repository or on [mods.factorio.com](https://mods.factorio.com/mod/nick-changer/discussion).

## <a name="feature"></a> Want a Feature?

You can *request* a new feature by [submitting an issue](https://gitlab.com/ZwerOxotnik/nick-changer/issues) to our GitLab
Repository or on [mods.factorio.com](https://mods.factorio.com/mod/nick-changer/discussion).

## Installing

If you have downloaded a zip archive:

* simply place it in your mods directory.

For more information, see [Installing Mods on the Factorio wiki](https://wiki.factorio.com/index.php?title=Installing_Mods).

If you have downloaded the source archive (GitLab):

* copy the mod directory into your factorio mods directory
* rename the mod directory to nick-changer_*versionnumber*, where *versionnumber* is the version of the mod that you've downloaded (e.g., 1.0.0)

## License

This project is copyright © 2019 ZwerOxotnik \<zweroxotnik@gmail.com\>.

Use of the source code included here is governed by the European Union Public License v. 1.2 only. See the [LICENSE](/LICENSE) file for details.

[homepage]: http://mods.factorio.com/mod/nick-changer
[Factorio]: https://factorio.com/
