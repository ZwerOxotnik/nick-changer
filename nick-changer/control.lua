--[[
Copyright (C) 2019 ZwerOxotnik <zweroxotnik@gmail.com>
Licensed under the EUPL, Version 1.2 only (the "LICENCE");
Author: ZwerOxotnik
Version: 1.2.2 (2019.11.27)

-- You can write and receive any information on the links below.
-- Source: https://gitlab.com/ZwerOxotnik/nick-changer
-- Mod portal: https://mods.factorio.com/mod/nick-changer
-- Homepage: https://forums.factorio.com/viewtopic.php?f=190&t=72983
]]--

local nick_changer = {}
nick_changer.self_events = require("nick-changer/self_events")
-- TODO: add handling mute, unmute player events

local function generate_new_nickname(name, attempt)
    if game.players[name .. "#" .. attempt] then
        return generate_new_nickname(name, attempt + 1)
    else
        return name .. "#" .. attempt
    end
end

local function set_new_nickname(player, new)
    if game.players[new] then
        new = generate_new_nickname(new, 1)
    end

    game.print(player.name .. " -> " .. new)
	player.name = new
	script.raise_event(nick_changer.self_events.on_new_nickname, {player_index = player.index})
end

local function check_real_nickname(player)
	local real_name = global.name_changer.players[player.index].real_name
	if real_name == player.name then return end

	set_new_nickname(player, real_name)
end

local function check_new_nickname(player, new)
	if string.len(new) > 200 then player.print({"nick-changer.too_much_long_nick"}) return end
    if new == player.name then return end
	if tonumber(new	) then player.print({"nick-changer.nick"}) return end

    set_new_nickname(player, new)
end

local function change_player_nickname(player)
	local new_nickname = player.mod_settings["newNickname"].value
	if new_nickname == "" then
		check_real_nickname(player)
	else
		check_new_nickname(player, new_nickname)
	end
end

local function check_global_data()
	global.name_changer = global.name_changer or {}
	local name_changer = global.name_changer
	name_changer.players = name_changer.players or {}

	-- -- Checks nicknames in the data
	for index, player in pairs(game.players) do
		if name_changer.players[index] == nil then
			name_changer.players[index] = {
				real_name = player.name
			}
		end
	end
end

local function on_player_created(event)
	-- Validation of data
	local player = game.players[event.player_index]
	if not (player and player.valid) then return end

	-- Memorizing real nickname
	local players_data = global.name_changer.players
	players_data[event.player_index] = {
		real_name = player.name
	}
end

local function on_player_joined_game(event)
	-- Validation of data
	local player = game.players[event.player_index]
	if not (player and player.valid) then return end

	local player_data = global.name_changer.players[event.player_index]
	if player_data == nil then
		player_data = {
			real_name = player.name
		}
	end

	nick_changer.fix_other_nickname(player)
	change_player_nickname(player)
end

local function on_player_removed(event)
	global.name_changer.players[event.player_index] = nil
end

nick_changer.fix_double_nickname = function(nickname)
	local player = game.players[nickname]
	if player == nil then return end

	nick_changer.restore_real_nickname(player)
end

nick_changer.fix_other_nickname = function(player)
	for _, other_player in pairs(game.players) do
		if (other_player ~= player and other_player.name == player.name) then
			nick_changer.restore_real_nickname(other_player)
		end
	end
end

nick_changer.restore_real_nickname = function(player)
	local player_data = global.name_changer.players[player.index]
	if player_data.real_name == player.name then return end

	nick_changer.fix_double_nickname(player_data.real_name)
	game.print(player.name .. " -> " .. player_data.real_name)
	player.name = player_data.real_name
	script.raise_event(nick_changer.self_events.on_new_nickname, {player_index = player.index})
	return true
end

local function on_player_left_game(event)
	local player = game.players[event.player_index]
	if not (player and player.valid) then return end

	nick_changer.restore_real_nickname(player)
end

local function on_runtime_mod_setting_changed(event)
	-- Validation of data
	if event.setting_type ~= "runtime-per-user" then return end
	local player = game.players[event.player_index]
	if not (player and player.valid) then return end

	if event.setting == "newNickname" then
		change_player_nickname(player)
	end
end

local function nickname_command(cmd)
	if cmd.player_index == nil then return end

	local player = game.players[cmd.player_index]
	if cmd.parameter == nil then
		if not nick_changer.restore_real_nickname(player) then
			player.print({"nick-changer.nick"})
			return
		end
	else
		check_new_nickname(player, cmd.parameter)
	end
end

nick_changer.add_commands = function()
	commands.add_command("nick", {"nick-changer.nick"}, nickname_command)
end

nick_changer.add_remote_interface = function()
	remote.remove_interface('nick-changer')
	remote.add_interface('nick-changer',
	{
		get_event_name = function(name)
			return nick_changer.self_events[name]
		end,
		get_real_name_by_player_index = function(player_index)
			local player_data = global.name_changer.players[player_index]
			if player_data then
				return player_data.real_name
			end
		end,
		set_new_nickname = check_new_nickname
	})
end

nick_changer.on_init = check_global_data
nick_changer.on_configuration_changed = check_global_data

nick_changer.events = {
	[defines.events.on_player_created] = on_player_created,
	[defines.events.on_player_joined_game] = on_player_joined_game,
	[defines.events.on_player_left_game] = on_player_left_game,
	[defines.events.on_player_removed] = on_player_removed,
	[defines.events.on_runtime_mod_setting_changed] = on_runtime_mod_setting_changed
}

return nick_changer
