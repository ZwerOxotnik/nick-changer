return {
	-- Called when a player successfully changed nickname.
	--	Contains:
	--		player_index :: uint: The index of the player.
	on_new_nickname = script.generate_event_name(),
}